### A Pluto.jl notebook ###
# v0.18.1

using Markdown
using InteractiveUtils

# ╔═╡ 90a41137-82b0-4986-84eb-f602b43a6949
using Pkg; Pkg.activate("Project.toml");

# ╔═╡ 2a99ddc6-0f27-45ed-b714-fe80712530c2
Pkg.add(["CSV", "DataFrames", "Statistics", "RDatasets", "StatsBase", "Plots", "VegaLite", "PlutoUI", "Flux"])

# ╔═╡ b4afb760-8e30-44f9-89bc-5951b2da1508
Pkg.add(["RDatasets"])

# ╔═╡ 7379f933-04d0-4879-9526-662922905e42
using CSV

# ╔═╡ b2d14761-0415-4299-9b4d-82592464abab
using DataFrames

# ╔═╡ c68a8fae-b319-4cd6-9422-f6e155c5a3d3
using PlutoUI

# ╔═╡ 75f48867-6f5c-4357-9f08-1d254234fb62
using RDatasets

# ╔═╡ 2562dba7-b202-445c-8087-ff3639d90a8f
using StatsBase

# ╔═╡ e6c82e83-bdfa-42d5-a1e4-50f8991ba4d5
using Statistics

# ╔═╡ 29da76b9-3cea-4b4e-8a23-9548b605b9df
using Plots

# ╔═╡ b351ebcc-908b-485b-bf4c-b0892ff89759
using Flux

# ╔═╡ b016685f-da7c-449d-a316-87912ee9a0c0
using Flux.Data

# ╔═╡ 24067fe7-405b-4e0f-8808-dbe1c489053e
using Flux: @epochs

# ╔═╡ 1274d0c8-bb9c-484d-adc3-90a3c256e412
md"# get the data"

# ╔═╡ 131aa160-2a89-4d4b-941f-c794262eb44e
begin
	real_df = CSV.read("C:\\Users\\mwand\\Documents\\JuliaProjects\\real_estate.csv", DataFrame)
end

# ╔═╡ 642ac6c3-0070-4761-8456-d89fae0fdcf9
md"## Clean the Data"

# ╔═╡ 3fc98d52-7a5c-4cc0-b887-67fc8e0650ce
begin
	rename!(real_df, Symbol.(replace.(string.(names(real_df)), Ref(r"\[m]"=>"_"))))
	rename!(real_df, Symbol.(replace.(string.(names(real_df)), Ref(r"\[s\]"=>"_"))))
	rename!(real_df, Symbol.(replace.(string.(names(real_df)), Ref(r"\s"=>"_"))))
end


# ╔═╡ 66d163e1-1b4b-41a3-b648-c9a1db43a88d
md"### Extracting the training and testing data from the dataset"

# ╔═╡ 8ec1d824-e3d8-4882-9be2-1af07d282e98
X = real_df[:,3:7]

# ╔═╡ 121f9b8c-eae7-478e-bf11-489f4861ef0d
cor(Matrix(real_df))

# ╔═╡ c3a9c74f-4bde-4f56-b378-858d0a075a21
another_data = Matrix(real_df[:,3:8])

# ╔═╡ 6d6e0feb-9b76-43da-9dcc-7c0951d8a857
function get_scaling_params(init_feature_mat)
	feature_mean = mean(init_feature_mat, dims = 1)
	f_dev = std(init_feature_mat, dims = 1)
	return (feature_mean, f_dev)
end

# ╔═╡ 39e065ba-b397-4341-89c3-4fbe8df1a571
function scaleFeat(feature_mat, sc_params)
	normalised_feature_mat = (feature_mat .- sc_params[1]) ./ sc_params[2]
end

# ╔═╡ 83f71c99-cb85-498c-bb18-b5a1d3dfc158
X_mat = Matrix(X)

# ╔═╡ 459b449e-a261-4171-a5bc-e585f228c69c
Y = real_df[:,8]

# ╔═╡ d549bbeb-2268-486c-9246-79bd16f5810f
all_data_size = size(X_mat)[1]

# ╔═╡ a012d6e1-ab4b-4b60-9952-19d09f11b3f9
training_size = 0.7

# ╔═╡ 6c9a8875-e76c-4746-bea1-3d3709da9bec
training_index = trunc(Int, training_size * all_data_size)

# ╔═╡ e130a71b-a727-4359-a657-ce3786a5850a
another_train = another_data[1:training_index, :]

# ╔═╡ 0eea1a46-eaa3-49ab-ab37-4a3f80858e74
another_test = another_data[1+training_index:end, :]

# ╔═╡ d6a2a003-c082-4f17-9285-4b8f259aeef2
sc_params = get_scaling_params(another_train[:, 1:end-1])

# ╔═╡ 17de9118-b78d-4590-9d8e-695ce2d45179
sc_mat_1 = scaleFeat(another_train[:, 1:end-1], sc_params)

# ╔═╡ 2cf7e064-0fb5-4eab-83b3-364e44b99ce3
sc_mat_2 = scaleFeat(another_test[:, 1:end-1], sc_params)

# ╔═╡ 35f4d8e7-4483-4a82-bc48-c1626fd4b804
train_feat = transpose(sc_mat_1)

# ╔═╡ b44bea81-d1e0-4806-bb35-8449a719acf8
test_feat = transpose(sc_mat_2)

# ╔═╡ 97391c20-8130-4175-9d64-3612aea5ca3a
train_label = another_train[:, end]

# ╔═╡ 34918754-38b0-4a0e-ad61-995e687fdafc
test_label = another_test[:, end]

# ╔═╡ e73fdc78-0c25-4942-94e8-73af549f410f
size(test_label)

# ╔═╡ e4673bfd-dc9b-4693-8ebc-76a4cdea0056
dl_train = DataLoader((train_feat, train_label), batchsize = 50)

# ╔═╡ f12867d3-7ebb-49dc-8278-bb1fdd8f9e06
dl_test = DataLoader((test_feat, test_label), batchsize = 10)

# ╔═╡ 2abce237-c805-4d05-b684-d3fa45c4e52a
X_mat_train = X_mat[1 : training_index, :]

# ╔═╡ 05685811-2c9f-439a-80b5-43ce5a70a817
size(X_mat_train)

# ╔═╡ d82e8b8c-9052-4b7f-8462-d86eb07ef79d
X_mat_test = X_mat[1 + training_index : end, :]

# ╔═╡ 35d8f670-8ee0-4711-93f1-1c30daf1657b
Y_vec_train = Y[1 : training_index]

# ╔═╡ 5eae8b38-7cb1-4084-b7c4-7935358084ae
size(Y_vec_train)

# ╔═╡ a2246ed3-033b-4767-ac63-1695cc2f19d7
Y_vec_test = Y[1 + training_index : end]

# ╔═╡ 68cf94b4-a55b-44cc-9404-77fe717ca006
# re_m = Dense(5, 1)
re_m = Chain(Dense(5, 2, σ), Dense(2, 1))

# ╔═╡ 641f21a0-407c-437f-8b27-12aa437da978
loss(x, y) = Flux.Losses.mse(re_m(x), y)

# ╔═╡ 2e0abf11-5bd2-4424-af36-1e7a7f405ff1
ps = Flux.params(re_m)

# ╔═╡ 26df7aaa-5a70-4763-ba3c-3ffe497c9bd3
opt = Descent(0.1)

# ╔═╡ 151b7143-4388-4de8-9db1-e41d311f8025
@epochs 30 Flux.train!(loss, ps, dl_train, opt)

# ╔═╡ 4112e34c-9d2a-49f9-ad00-ce6650502ddf
pred2 = re_m(test_feat)

# ╔═╡ 82f6585a-5842-4297-a538-b8d7faafd8ab
function root_mean_square_error(actual_outcome, predicted_outcome)
	errors = predicted_outcome - actual_outcome
	squared_errors = errors .^ 2
	mean_squared_errors = mean(squared_errors)
	rmse = sqrt(mean_squared_errors)
	return rmse
end

# ╔═╡ 6f88df04-feab-429d-a062-3d9ac4b6162b
root_mean_square_error(test_label, vec(pred2))

# ╔═╡ Cell order:
# ╠═90a41137-82b0-4986-84eb-f602b43a6949
# ╠═2a99ddc6-0f27-45ed-b714-fe80712530c2
# ╠═7379f933-04d0-4879-9526-662922905e42
# ╠═b2d14761-0415-4299-9b4d-82592464abab
# ╠═c68a8fae-b319-4cd6-9422-f6e155c5a3d3
# ╠═b4afb760-8e30-44f9-89bc-5951b2da1508
# ╠═75f48867-6f5c-4357-9f08-1d254234fb62
# ╠═2562dba7-b202-445c-8087-ff3639d90a8f
# ╠═e6c82e83-bdfa-42d5-a1e4-50f8991ba4d5
# ╠═29da76b9-3cea-4b4e-8a23-9548b605b9df
# ╠═b351ebcc-908b-485b-bf4c-b0892ff89759
# ╠═b016685f-da7c-449d-a316-87912ee9a0c0
# ╠═24067fe7-405b-4e0f-8808-dbe1c489053e
# ╠═1274d0c8-bb9c-484d-adc3-90a3c256e412
# ╠═131aa160-2a89-4d4b-941f-c794262eb44e
# ╠═642ac6c3-0070-4761-8456-d89fae0fdcf9
# ╠═3fc98d52-7a5c-4cc0-b887-67fc8e0650ce
# ╠═74003770-684a-4ccb-bd70-a42a7fc6ff30
# ╠═66d163e1-1b4b-41a3-b648-c9a1db43a88d
# ╠═8ec1d824-e3d8-4882-9be2-1af07d282e98
# ╠═121f9b8c-eae7-478e-bf11-489f4861ef0d
# ╠═c3a9c74f-4bde-4f56-b378-858d0a075a21
# ╠═6d6e0feb-9b76-43da-9dcc-7c0951d8a857
# ╠═39e065ba-b397-4341-89c3-4fbe8df1a571
# ╠═83f71c99-cb85-498c-bb18-b5a1d3dfc158
# ╠═459b449e-a261-4171-a5bc-e585f228c69c
# ╠═d549bbeb-2268-486c-9246-79bd16f5810f
# ╠═a012d6e1-ab4b-4b60-9952-19d09f11b3f9
# ╠═6c9a8875-e76c-4746-bea1-3d3709da9bec
# ╠═e130a71b-a727-4359-a657-ce3786a5850a
# ╠═0eea1a46-eaa3-49ab-ab37-4a3f80858e74
# ╠═d6a2a003-c082-4f17-9285-4b8f259aeef2
# ╠═17de9118-b78d-4590-9d8e-695ce2d45179
# ╠═2cf7e064-0fb5-4eab-83b3-364e44b99ce3
# ╠═35f4d8e7-4483-4a82-bc48-c1626fd4b804
# ╠═b44bea81-d1e0-4806-bb35-8449a719acf8
# ╠═97391c20-8130-4175-9d64-3612aea5ca3a
# ╠═34918754-38b0-4a0e-ad61-995e687fdafc
# ╠═e73fdc78-0c25-4942-94e8-73af549f410f
# ╠═e4673bfd-dc9b-4693-8ebc-76a4cdea0056
# ╠═f12867d3-7ebb-49dc-8278-bb1fdd8f9e06
# ╠═2abce237-c805-4d05-b684-d3fa45c4e52a
# ╠═05685811-2c9f-439a-80b5-43ce5a70a817
# ╠═d82e8b8c-9052-4b7f-8462-d86eb07ef79d
# ╠═35d8f670-8ee0-4711-93f1-1c30daf1657b
# ╠═5eae8b38-7cb1-4084-b7c4-7935358084ae
# ╠═a2246ed3-033b-4767-ac63-1695cc2f19d7
# ╠═68cf94b4-a55b-44cc-9404-77fe717ca006
# ╠═641f21a0-407c-437f-8b27-12aa437da978
# ╠═2e0abf11-5bd2-4424-af36-1e7a7f405ff1
# ╠═26df7aaa-5a70-4763-ba3c-3ffe497c9bd3
# ╠═151b7143-4388-4de8-9db1-e41d311f8025
# ╠═4112e34c-9d2a-49f9-ad00-ce6650502ddf
# ╠═82f6585a-5842-4297-a538-b8d7faafd8ab
# ╠═6f88df04-feab-429d-a062-3d9ac4b6162b
